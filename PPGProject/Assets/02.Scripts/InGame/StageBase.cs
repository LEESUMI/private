﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageBase : MonoBehaviour
{
    protected Transform MapGroup;
    protected StageInfo StageInfo = null;

    private void Start()
    {
        if (!GameMgr.instance.IsGameStart)
        {
            SceneMgr.instance.GoScene("TitleScene", null);
            return;
        }

        Init();
    }

    #region CREATE_MAP
    private void CreateMap()
    {
        int section = StageInfo.TotalSection;
        GameMgr.instance.StageInfo.RunSpeed = 10;

        // 배경
        GameObject oBG = Instantiate(Resources.Load("InGame/Map/InGame_BG")) as GameObject;
        oBG.name = "BG";
        oBG.transform.SetParent(transform);
        oBG.transform.localPosition = new Vector3(0f, 12f, 50);
        oBG.transform.localScale = new Vector3(2048f, 1080f, 1f) / 50;

        oBG.GetComponent<InGame_BG>().Init(StageInfo.MapType);

        // 처음 시작 하는부분의 뒷부분 바닥을 만든다
        float bottomPosY = 0f;
        float underBottomPosY = -11f;
        int makeBottomCnt = -1;
        CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_Bottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), bottomPosY);
        CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_UnderBottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), underBottomPosY);
        makeBottomCnt++;

        // section이 0이하이면 바로 보스전 합니다
        if (section > 0)
        {
            for (int i = 0; i <= section; ++i)
            {
                if (i == 0)
                {
                    // 지상
                    CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_Begin_Bottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), bottomPosY);

                    // 지하
                    CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_Begin_UnderBottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), underBottomPosY);
                }
                else
                {
                    // 지상
                    Transform parent = CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_Bottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), bottomPosY);
                    CreateMiddleMap(parent, i);

                    // 지하
                    Transform parentUnder = CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_UnderBottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), underBottomPosY);
                    CreateUnderMiddleMap(parentUnder, i);

                    // 몬스터 소환
                    CreateMonster(parent.transform.position.x + 1, parent.transform.position.x + 29, parent.transform.position.y);
                    CreateMonster(parentUnder.transform.position.x + 1, parentUnder.transform.position.x + 29, parentUnder.transform.position.y);
                }

                makeBottomCnt++;
            }
        }

        // 보스 바닥을 만든다
        CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_Boss_Bottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), bottomPosY);
        CreateBottomTile(makeBottomCnt, string.Format("InGame/Map/{0}/{1}_Map_Boss_UnderBottom", StageInfo.MapType.ToString(), StageInfo.MapType.ToString()), underBottomPosY);
        makeBottomCnt++;
    }

    private Transform CreateBottomTile(int makeBottomCnt, string mapBottom, float posY)
    {
        GameObject oMapBottom = Instantiate(Resources.Load(mapBottom)) as GameObject;
        oMapBottom.name = "Section_" + makeBottomCnt;
        oMapBottom.transform.SetParent(MapGroup);
        oMapBottom.transform.localPosition = new Vector3(makeBottomCnt * 108, posY, 0);
        oMapBottom.transform.localScale = Vector3.one;

        return oMapBottom.transform;
    }

    private void CreateMiddleMap(Transform parent, int section)
    {
        if (parent == null)
            return;

        // 중간 맵 생성
        CreateMiddleTile(parent, section, 5.8f);

        // 상단 맵 생성
        CreateMiddleTile(parent, section, 10.6f);
    }

    private void CreateMiddleTile(Transform parent, int section, float posY)
    {
        StageSectionInfo sectionInfo = DataMgr.instance.GetStageSectionInfo(StageInfo.Id, section);
        if (sectionInfo == null)
        {
            Debug.LogError(string.Format("error stage section info is null stage id : {0}, section : {1}", StageInfo.Id, section));
            return;
        }

        int makeMiddleGap = 0;
        int createMiddleCnt = Random.Range(sectionInfo.Min_CreateMiddleCount_Section, sectionInfo.Max_CreateMiddleCount_Section + 1);

        for (int i = 0; i < createMiddleCnt; ++i)
        {
            // 구간의 앞부분 랜덤하게 띄우기
            makeMiddleGap += Random.Range(0, 8);

            int piece = Random.Range(3, 8);
            string mapMiddle = string.Format("InGame/Map/{0}/{1}_Map_Middle_Piece{2}", StageInfo.MapType.ToString(), StageInfo.MapType.ToString(), piece);
            GameObject oMapMiddle = Instantiate(Resources.Load(mapMiddle)) as GameObject;
            oMapMiddle.transform.SetParent(parent);
            oMapMiddle.transform.localPosition = new Vector3(makeMiddleGap, posY, 0);
            oMapMiddle.transform.localScale = Vector3.one;

            // 구간에서 생성한 중간 맵길이 만큼 띄우기
            makeMiddleGap += piece;

            // 몬스터 소환
            CreateMonster(oMapMiddle.transform.position.x + 1, oMapMiddle.transform.position.x + piece - 1, posY);
        }
    }

    private void CreateUnderMiddleMap(Transform parent, int section)
    {
        if (parent == null)
            return;

        // 중간 맵 생성
        CreateUnderMiddleTile(parent, section, 5.8f);
    }

    private void CreateUnderMiddleTile(Transform parent, int section, float posY)
    {
        StageSectionInfo sectionInfo = DataMgr.instance.GetStageSectionInfo(StageInfo.Id, section);
        if (sectionInfo == null)
        {
            Debug.LogError(string.Format("error stage section info is null stage id : {0}, section : {1}", StageInfo.Id, section));
            return;
        }

        int makeMiddleGap = 0;
        int createMiddleCnt = Random.Range(sectionInfo.Min_CreateUnderMiddleCount_Section, sectionInfo.Max_CreateUnderMiddleCount_Section + 1);

        for (int i = 0; i < createMiddleCnt; ++i)
        {
            // 구간의 앞부분 랜덤하게 띄우기
            makeMiddleGap += Random.Range(0, 8);

            int piece = Random.Range(3, 8);
            string mapMiddle = string.Format("InGame/Map/{0}/{1}_Map_UnderMiddle_Piece{2}", StageInfo.MapType.ToString(), StageInfo.MapType.ToString(), piece);
            GameObject oMapMiddle = Instantiate(Resources.Load(mapMiddle)) as GameObject;
            oMapMiddle.transform.SetParent(parent);
            oMapMiddle.transform.localPosition = new Vector3(makeMiddleGap, posY, 0);
            oMapMiddle.transform.localScale = Vector3.one;

            // 구간에서 생성한 중간 맵길이 만큼 띄우기
            makeMiddleGap += piece;

            // 몬스터 소환
            CreateMonster(oMapMiddle.transform.position.x + 1, oMapMiddle.transform.position.x + piece - 1, posY);
        }
    }

    private void CreateMonster(float minPosX, float maxPosX, float posY)
    {
        //if (Random.Range(0, 1) != 0)
        //    return;

        //GameObject oMonster = Instantiate(Resources.Load("InGame/InGameMonster")) as GameObject;
        //oMonster.transform.SetParent(transform);
        //oMonster.transform.position = new Vector3(Random.Range(minPosX, maxPosX), posY, 0f);
        //oMonster.transform.localScale = Vector3.one;

        //InGameMonster sMonster = oMonster.GetComponent<InGameMonster>();
        //sMonster.Init();
    }
    #endregion // CREATE_MAP

    public virtual void Init()
    {
        GameObject oMapGroup = GameObject.Find("MapGroup");
        if (oMapGroup != null)
            MapGroup = oMapGroup.transform;

        if (MapGroup == null)
        {
            MapGroup = new GameObject("MapGroup").transform;
            MapGroup.SetParent(transform);
            MapGroup.transform.localPosition = Vector3.zero;
            MapGroup.transform.localScale = Vector3.one;
        }

        StageInfo = GameMgr.instance.StageInfo;
        if (StageInfo == null)
        {
            Debug.LogError("error stage info is null");
            return;
        }

        CreateMap();

        PlayerController.instance.Init(this, transform);

        GameStart();
    }

    public virtual void GameStart()
    {
        StartCoroutine(_GameStart());
    }

    private IEnumerator _GameStart()
    {
        for (int i = 0; i < StageInfo.StartDelay; ++i)
        {
            Debug.Log(string.Format("{0}초 후 게임이 시작 됩니다", StageInfo.StartDelay - i));

            yield return new WaitForSeconds(1f);
        }

        PlayerController.instance.IsIsPlaying = true;
    }

    private void Update()
    {
        if (PlayerController.instance.IsIsPlaying)
        {
            if (MapGroup == null)
                return;

            if (PlayerController.instance.InGamePlayer.IsBossBattle)
                return;

            MapGroup.Translate(-Vector3.right * GameMgr.instance.StageInfo.RunSpeed * Time.deltaTime);
        }
    }

    public virtual void GameFinish()
    {
    }

    public virtual void EnterBoss()
    {
        Debug.Log("보스 배틀 진입 했습니다");
        StartCoroutine(PlayerController.instance.InGamePlayerCamera._BossBattleMoveCamera(()=> 
        {
            Debug.Log("보스 전투를 시작 합니다!!!");
            Debug.Log("보스 전투를 시작 합니다!!!");
        }));
    }
}
