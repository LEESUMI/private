﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : Immortal<PlayerController>
{
    public bool IsIsPlaying = false;
    public StageBase StageBase { get; private set; }

    public InGamePlayer InGamePlayer { get; private set; }
    public InGamePlayerCamera InGamePlayerCamera { get; private set; }

    private System.Action UpdateHeart = null;
    private System.Action UpdateStatus = null;

    public void Init(StageBase stageBase, Transform parent)
    {
        IsIsPlaying = false;
        StageBase = stageBase;

        // 플레이어 생성
        GameObject oInGamePlayer = Instantiate(Resources.Load("InGame/InGamePlayer")) as GameObject;
        oInGamePlayer.transform.SetParent(parent);
        oInGamePlayer.transform.localPosition = new Vector3(0f, 3f, 0f);
        oInGamePlayer.transform.localScale = Vector3.one;

        InGamePlayer = oInGamePlayer.GetComponent<InGamePlayer>();
        InGamePlayer.Init();

        // 플레이어 카메라 생성
        GameObject oInGamePlayerCamera = Instantiate(Resources.Load("InGame/InGamePlayerCamera")) as GameObject;
        oInGamePlayerCamera.transform.SetParent(parent);
        oInGamePlayerCamera.transform.localPosition = Vector3.zero;
        oInGamePlayerCamera.transform.localScale = Vector3.one;

        InGamePlayerCamera = oInGamePlayerCamera.GetComponent<InGamePlayerCamera>();
        InGamePlayerCamera.Init(InGamePlayer);

        UIInGamePanel inGamePanel = UIMgr.instance.Open("UIInGamePanel") as UIInGamePanel;
        inGamePanel.SetEventFunc(UpdateHeart, UpdateStatus);
    }
}
