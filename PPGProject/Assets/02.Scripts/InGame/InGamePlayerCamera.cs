﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePlayerCamera : MonoBehaviour
{
    private InGamePlayer Target;

    public void Init(InGamePlayer target)
    {
        Target = target;
    }

    private void LateUpdate()
    {
        if (Target == null)
            return;

        if (Target.IsBossBattle)
            return;

        Vector3 pos = Vector3.Lerp(transform.position, Target.transform.position, 10 * Time.deltaTime);
        pos.z = -50;
        // 지상
        if (Target.transform.localPosition.y > 0f)
        {
            // 지상 바닥
            if (pos.y < 4.8)
                pos.y = 4.8f;
            // 지상 천장
            else if (pos.y > 16f)
                pos.y = 16f;
        }
        // 지하
        else
        {
            // 지하 바닥
            if (pos.y < -6f)
                pos.y = -6f;
        }

        transform.position = pos;
        //transform.position = new Vector3(Target.transform.position.x, posY, Target.transform.position.z - 100);
    }

    public IEnumerator _BossBattleMoveCamera(System.Action callback)
    {
        Vector3 startPos = transform.position;
        Vector3 endPos = new Vector3(transform.position.x + 7, transform.position.y, transform.position.z);
        float moveProgress = 0f;
        while (true)
        {
            moveProgress += 1f * Time.deltaTime;
            transform.position = Vector3.Lerp(startPos, endPos, moveProgress);

            if (moveProgress >= 1f)
                break;

            yield return null;
        }

        if (callback != null)
            callback();
    }
}
