﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScrolling : MonoBehaviour
{
    private MeshRenderer BGRenderer;
    private float SpeedRate = 1f;

    public void Init(float speedRate, Material material)
    {
        SpeedRate = speedRate;
        BGRenderer = gameObject.GetComponent<MeshRenderer>();
        BGRenderer.material = material;

        Debug.Log(speedRate);
    }

    public void UpdateScrolling()
    {
        Vector2 offset = BGRenderer.material.mainTextureOffset;
        offset += new Vector2((GameMgr.instance.StageInfo.RunSpeed / 100f) * SpeedRate * Time.deltaTime, 0f);
        if (offset.x > 1)
            offset.x -= 1;

        BGRenderer.material.mainTextureOffset = offset;
    }
}
