﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGameMonster : MonoBehaviour
{
    private Rigidbody2D Rigid;
    public InGameMonsterState InGameMonsterState { get; private set; }

    private GameObject Model;

    public void Init()
    {
        InGameMonsterState = InGameMonsterState.Idle;
        Rigid = GetComponent<Rigidbody2D>();

        CreateModel();
    }

    private void CreateModel()
    {
        if (Model != null)
            Destroy(Model);
        Model = null;

        List<string> listModel = new List<string>()
        {
            "Ghost",
            "Scutom",
        };

        string modelName = listModel[Random.Range(0, listModel.Count)];
        Model = Instantiate(Resources.Load("Character/Monster/" + modelName)) as GameObject;
        Model.name = modelName;
        Model.transform.SetParent(transform);
        Model.transform.localPosition = Vector3.zero;
        Model.transform.localScale = Vector3.one;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Player"))
        {
            gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag.Equals("Floor"))
        {
            Rigid.gravityScale = 0f;
            Rigid.velocity = Vector2.zero;
            gameObject.GetComponent<CapsuleCollider2D>().isTrigger = true;
        }
    }
}
