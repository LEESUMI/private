﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGamePlayer : MonoBehaviour
{
    private Rigidbody2D Rigid;
    public InGamePlayerState InGamePlayerState { get; private set; }
    public bool IsBossBattle { get; private set; }

    public void Init()
    {
        InGamePlayerState = InGamePlayerState.Run;
        IsBossBattle = false;
        Rigid = GetComponent<Rigidbody2D>();
    }

    public void Jump()
    {
        if (Rigid == null)
            return;

        if (InGamePlayerState == InGamePlayerState.Jump)
            return;

        Rigid.AddForce(Vector3.up * 1300);
        InGamePlayerState = InGamePlayerState.Jump;
    }

    public void Down()
    {
        Debug.Log("Dpwn");
    }

    public void Attack()
    {
        Debug.Log("Attack");
    }

    public void SpecialAttack()
    {
        Debug.Log("SpecialAttack");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("EnterBoss"))
        {
            IsBossBattle = true;

            PlayerController.instance.StageBase.EnterBoss();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag.Equals("Floor"))
        {
            InGamePlayerState = InGamePlayerState.Run;
        }
    }
}
