﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InGame_BG : MonoBehaviour
{
    public List<Material> ListForestBG;
    public List<BGScrolling> ListBGRenderer;

    private float BG1_SpeedRate = 1f;
    private float BG2_SpeedRate = 0.8f;
    private float BG3_SpeedRate = 0.5f;

    public void Init(MapType mapType)
    {
        Debug.Log(mapType.ToString());

        List<Material> listMaterial = null;
        switch (mapType)
        {
            case MapType.Forest:
                listMaterial = ListForestBG;
                break;
        }

        if (listMaterial == null)
        {
            Debug.LogError("listMaterial is null");
            return;
        }

        for (int i = 0; i < ListBGRenderer.Count; ++i)
        {
            if (listMaterial.Count <= i || listMaterial[i] == null)
                continue;

            float rate = 0f;
            switch (i)
            {
                case 0:
                    rate = BG1_SpeedRate;
                    break;

                case 1:
                    rate = BG2_SpeedRate;
                    break;

                case 2:
                    rate = BG3_SpeedRate;
                    break;
            }

            ListBGRenderer[i].Init(rate, listMaterial[i]);
        }
    }

    private void Update()
    {
        if (!PlayerController.instance.IsIsPlaying)
            return;

        if (PlayerController.instance.InGamePlayer.IsBossBattle)
            return;

        for (int i = 0; i < ListBGRenderer.Count; ++i)
            ListBGRenderer[i].UpdateScrolling();
    }
}
