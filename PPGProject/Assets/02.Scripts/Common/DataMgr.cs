﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using System.Linq;

public class DataMgr : Immortal<DataMgr>
{
    private Dictionary<int, StageInfo> DicStageInfo = new Dictionary<int, StageInfo>();
    private Dictionary<int, List<StageSectionInfo>> DicStageSectionInfo = new Dictionary<int, List<StageSectionInfo>>();

    #region LOAD_DATA
    public IEnumerator _LoadDataTable()
    {
        yield return StartCoroutine(_Load_StageInfo());
        yield return StartCoroutine(_Load_StageSectionInfo()); // after _Load_StageInfo
    }

    private IEnumerator _Load_StageInfo()
    {
        yield return null;
        DicStageInfo.Clear();

        TextAsset textAsset = Resources.Load("DataTables/StageInfoTable") as TextAsset;
        List<StageInfo> list = JsonConvert.DeserializeObject<List<StageInfo>>(textAsset.text);

        for (int i = 0; i < list.Count; ++i)
        {
            if (list[i].StartDelay < 0)
            {
                Debug.LogError(string.Format("error _Load_StageInfo id : {0}, StartDelay : {1}", list[i].Id, list[i].StartDelay));
            }

            if (list[i].TotalSection < 0)
            {
                Debug.LogError(string.Format("error _Load_StageInfo id : {0}, TotalSection : {1}", list[i].Id, list[i].TotalSection));
            }

            if (list[i].RunSpeed <= 0)
            {
                Debug.LogError(string.Format("error _Load_StageInfo id : {0}, RunSpeed : {1}", list[i].Id, list[i].RunSpeed));
            }

            DicStageInfo.Add(list[i].Id, list[i]);
        }


        Debug.Log(JsonConvert.SerializeObject(DicStageInfo));
    }

    private IEnumerator _Load_StageSectionInfo()
    {
        yield return null;
        DicStageSectionInfo.Clear();

        TextAsset textAsset = Resources.Load("DataTables/StageSectionInfoTable") as TextAsset;
        List<StageSectionInfo> list = JsonConvert.DeserializeObject<List<StageSectionInfo>>(textAsset.text);

        for(int i = 0; i < list.Count; ++i)
        {
            if (GetStageInfoById(list[i].Id) == null)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0} StageInfo is null", list[i].Id));
            }

            if (list[i].Section < 0)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Section : {1}", list[i].Id, list[i].Section));
            }

            if (list[i].Min_CreateMiddleCount_Section < 0 || list[i].Min_CreateMiddleCount_Section > 2)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Min_CreateMiddleCount_Section : {1}", list[i].Id, list[i].Min_CreateMiddleCount_Section));
            }

            if (list[i].Max_CreateMiddleCount_Section < 0 || list[i].Max_CreateMiddleCount_Section > 2)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Max_CreateMiddleCount_Section : {1}", list[i].Id, list[i].Max_CreateMiddleCount_Section));
            }

            if (list[i].Min_CreateMiddleCount_Section > list[i].Max_CreateMiddleCount_Section)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Min_CreateMiddleCount_Section : {1}, Max_CreateMiddleCount_Section : {2}", list[i].Id, list[i].Min_CreateMiddleCount_Section, list[i].Max_CreateMiddleCount_Section));
            }

            if (list[i].Min_CreateUnderMiddleCount_Section < 0 || list[i].Min_CreateUnderMiddleCount_Section > 2)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Min_CreateUnderMiddleCount_Section : {1}", list[i].Id, list[i].Min_CreateUnderMiddleCount_Section));
            }

            if (list[i].Max_CreateUnderMiddleCount_Section < 0 || list[i].Max_CreateUnderMiddleCount_Section > 2)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Max_CreateUnderMiddleCount_Section : {1}", list[i].Id, list[i].Max_CreateUnderMiddleCount_Section));
            }

            if (list[i].Min_CreateUnderMiddleCount_Section > list[i].Max_CreateUnderMiddleCount_Section)
            {
                Debug.LogError(string.Format("error _Load_StageSectionInfo id : {0}, Min_CreateUnderMiddleCount_Section : {1}, Max_CreateUnderMiddleCount_Section : {2}", list[i].Id, list[i].Min_CreateUnderMiddleCount_Section, list[i].Max_CreateUnderMiddleCount_Section));
            }

            if (!DicStageSectionInfo.ContainsKey(list[i].Id) || DicStageSectionInfo[list[i].Id] == null)
                DicStageSectionInfo[list[i].Id] = new List<StageSectionInfo>();

            DicStageSectionInfo[list[i].Id].Add(list[i]);
        }

        foreach(List<StageSectionInfo> infoList in DicStageSectionInfo.Values)
        {
            infoList.Sort(delegate (StageSectionInfo tmp1, StageSectionInfo tmp2)
            {
                return tmp1.Section.CompareTo(tmp2.Section);
            });
        }

        foreach (int id in DicStageSectionInfo.Keys)
        {
            if (DicStageSectionInfo[id].Count != DicStageInfo[id].TotalSection)
            {
                Debug.LogError("error _Load_StageSectionInfo DicStageSectionInfo[id].Count != DicStageInfo[id].TotalSection");
            }
        }

        Debug.Log(JsonConvert.SerializeObject(DicStageSectionInfo));
    }
    #endregion // LOAD_DATA

    public StageInfo GetStageInfoById(int stageId)
    {
        if (DicStageInfo.ContainsKey(stageId))
            return DicStageInfo[stageId];

        return null;
    }

    public List<StageInfo> GetListStageInfo()
    {
        return DicStageInfo.Values.ToList();
    }

    public StageSectionInfo GetStageSectionInfo(int stageId, int section)
    {
        if (!DicStageSectionInfo.ContainsKey(stageId))
            return null;

        return DicStageSectionInfo[stageId].Find(data => data.Section == section);
    }
}
