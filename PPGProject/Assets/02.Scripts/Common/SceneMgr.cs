﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneMgr : Immortal<SceneMgr>
{
    public void GoScene(string sceneName, System.Action finishCallBack, System.Action beginCallBack = null)
    {
        StartCoroutine(_GoScene(sceneName, finishCallBack, beginCallBack));
    }

    IEnumerator _GoScene(string sceneName, System.Action finishCallBack, System.Action beginCallBack)
    {
        if (beginCallBack != null)
            beginCallBack();

        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
        while (!async.isDone)
            yield return null;

        if (finishCallBack != null)
            finishCallBack();
    }
}
