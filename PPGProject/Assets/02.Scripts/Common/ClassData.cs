﻿
public class StageInfo
{
    public int Id;
    public MapType MapType;
    public int StartDelay;
    public int TotalSection;
    public float RunSpeed;
}

public class StageSectionInfo
{
    public int Id;
    public int Section;
    public int Min_CreateMiddleCount_Section;
    public int Max_CreateMiddleCount_Section;
    public int Min_CreateUnderMiddleCount_Section;
    public int Max_CreateUnderMiddleCount_Section;
}