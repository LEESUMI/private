﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIMgr : Immortal<UIMgr>
{
    private const string BASE_PATH = "UI/";
    private Transform BaseParent, PopupParent, ToastParent, LoadingParent;

    [SerializeField]
    private List<UIBasePanel> ListUIPanel = new List<UIBasePanel>();
    [SerializeField]
    private UIBasePanel CurPanel = null;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Prev();
        }
    }

    private void CreateUICanvas()
    {
        GameObject oCanvas = GameObject.FindGameObjectWithTag("uicanvas");
        if (oCanvas == null)
            oCanvas = Instantiate(Resources.Load(BASE_PATH + "UICanvas")) as GameObject;

        if (BaseParent == null)
        {
            BaseParent = oCanvas.transform.Find("BaseParent");
        }

        if (PopupParent == null)
        {
            PopupParent = oCanvas.transform.Find("PopupParent");
        }

        if (ToastParent == null)
        {
            ToastParent = oCanvas.transform.Find("ToastParent");
        }

        if (LoadingParent == null)
        {
            LoadingParent = oCanvas.transform.Find("LoadingParent");
        }
    }

    public UIBasePanel Open(string panelName, params object[] param)
    {
        UIBasePanel panel = GetPanel(panelName);
        if (panel == null)
        {
            CreateUICanvas();

            GameObject oPanel = Instantiate(Resources.Load(BASE_PATH + panelName)) as GameObject;
            oPanel.name = panelName;

            panel = oPanel.GetComponent<UIBasePanel>();
            switch (panel.PanelType)
            {
                case PanelType.Normal:
                    oPanel.transform.SetParent(BaseParent);
                    break;
            }
            oPanel.transform.localPosition = Vector3.zero;
            oPanel.transform.localScale = Vector3.one;

            panel.Init();
            ListUIPanel.Insert(0, panel);
        }

        panel.SetParameters(param);
        panel.LateInit();
        CurPanel = panel;

        OpenEvent(panel);

        return panel;
    }

    public UIBasePanel GetPanel(string panelName)
    {
        return ListUIPanel.Find(data => data.name.Equals(panelName));
    }

    public void Prev()
    {
        if (ListUIPanel.Count <= 1 || CurPanel == null || CurPanel is UITitlePanel || CurPanel is UILobbyPanel)
        {
            OpenPopup("게임을 종료 하시겠습니까?", () =>
            {
                Application.Quit();

            }, () => { }, "예", "아니오");
            return;
        }

        int curIdx = ListUIPanel.FindIndex(data => data.name.Equals(CurPanel.name));
        int nextIdx = ListUIPanel.FindIndex(curIdx + 1, data => data.PanelType == PanelType.Normal);

        if (curIdx > -1 && nextIdx > -1)
        {
            UIBasePanel curPanel = ListUIPanel[curIdx];
            UIBasePanel nextPanel = ListUIPanel[nextIdx];

            switch (curPanel.GetPrevType())
            {
                case PrevType.Hide:
                    curPanel.Hide();
                    break;

                case PrevType.Close:
                    curPanel.Close();
                    break;
            }

            nextPanel.Show();
            CurPanel = nextPanel;
        }
    }

    public void OpenEvent(UIBasePanel basePanel)
    {
        ListUIPanel.Remove(basePanel);
        ListUIPanel.Insert(0, basePanel);
    }

    public void HideEvent(UIBasePanel basePanel)
    {
        ListUIPanel.Remove(basePanel);
        ListUIPanel.Add(basePanel);
    }

    public void CloseEvent(UIBasePanel basePanel)
    {
        int curIdx = ListUIPanel.FindIndex(data => data.name.Equals(basePanel.name));
        int nextIdx = -1;
        if (ListUIPanel.Count > curIdx + 1)
            nextIdx = ListUIPanel.FindIndex(curIdx + 1, data => data.PanelType == PanelType.Normal);

        if (nextIdx > -1)
            CurPanel = ListUIPanel[nextIdx];

        ListUIPanel.Remove(basePanel);

        if (CurPanel == null)
        {
            if (ListUIPanel.Count > 0)
                CurPanel = ListUIPanel[0];
        }
    }

    public void ShutDown()
    {
        for (int i = 0; i < ListUIPanel.Count;)
            ListUIPanel[i].Close();

        ListUIPanel.Clear();
    }

    private UIToastMessagePanel _UIToastMessagePanel = null;
    public void OpenToastPopup(string msg)
    {
        if (_UIToastMessagePanel == null)
        {
            GameObject oPanel = Instantiate(Resources.Load(BASE_PATH + "UIToastMessagePanel")) as GameObject;
            oPanel.name = "UIToastMessagePanel";
            oPanel.transform.SetParent(ToastParent);
            oPanel.transform.localPosition = Vector3.zero;
            oPanel.transform.localScale = Vector3.one;

            _UIToastMessagePanel = oPanel.GetComponent<UIToastMessagePanel>();
        }
        _UIToastMessagePanel.Init(msg);
    }

    public void OpenPopup(string msg, System.Action btn1CallBack, System.Action btn2CallBack, string btn1Name, string btn2Name)
    {
        UIPopupPanel popup = Open("UIPopupPanel") as UIPopupPanel;
        if (popup != null)
        {
            popup.transform.SetParent(PopupParent);
            popup.transform.localPosition = Vector3.zero;
            popup.transform.localScale = Vector3.one;
            popup.OpenPopup(msg, btn1CallBack, btn2CallBack, btn1Name, btn2Name);
        }
    }

    public void OpenLoading(bool open)
    {
    }
}
