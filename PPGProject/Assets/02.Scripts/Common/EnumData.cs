﻿public enum BuildType
{
    Local,
    Develop,
    Live,
}

public enum LogType
{
    Normal,
    Warning,
    Error,
}

public enum PrevType
{
    None,
    Hide,
    Close,
}

public enum PanelType
{
    Normal,
    Ignore,
}

public enum MapType
{
    None,
    Forest,
}

public enum InGamePlayerState
{
    Run,
    Jump,
}

public enum InGameMonsterState
{
    Idle,
    Attack,
    Die,
}