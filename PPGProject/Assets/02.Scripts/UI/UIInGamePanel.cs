﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInGamePanel : UIBasePanel
{
    private GameObject SettingBtn, UpBtn, DownBtn, SpecialBtn, AttackBtn;

    public override void Init()
    {
        base.Init();
    }

    public void SetEventFunc(System.Action updateHeart, System.Action updateStatus)
    {
        updateHeart = UpdateHeart;
        updateStatus = UpdateStatus;
    }

    public override void LateInit()
    {
        base.LateInit();
    }

    public void OnClickSettingBtn()
    {
        Time.timeScale = 0f;

        UIMgr.instance.OpenPopup("로비로 돌아 가시겠습니까?", () =>
        {
            Time.timeScale = 1f;

            UIMgr.instance.ShutDown();
            UIMgr.instance.OpenLoading(true);
            SceneMgr.instance.GoScene("MainScene", () =>
            {
                UIMgr.instance.Open("UILobbyPanel");

                UIMgr.instance.OpenLoading(false);
            });

        }, () => { Time.timeScale = 1f; }, "로비가기", "계속하기");
    }

    private void UpdateHeart()
    {

    }

    private void UpdateStatus()
    {
    }

    private void UpdateCurrentPosition()
    {
    }

    public void OnClickUpBtn()
    {
        PlayerController.instance.InGamePlayer.Jump();
    }

    public void OnClickDownBtn()
    {
        PlayerController.instance.InGamePlayer.Down();
    }

    public void OnClickSpecialBtn()
    {
        PlayerController.instance.InGamePlayer.SpecialAttack();
    }

    public void OnClickAttackBtn()
    {
        PlayerController.instance.InGamePlayer.Attack();
    }
}
