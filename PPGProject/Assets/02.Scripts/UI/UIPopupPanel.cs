﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupPanel : UIBasePanel
{
    public GameObject Btn1, Btn2;
    public Text MsgTxt, Btn1NameTxt, Btn2NameTxt;
    System.Action Btn1CallBack, Btn2CallBack;

    public override PrevType GetPrevType()
    {
        return PrevType.Close;
    }

    public void OpenPopup(string msg, System.Action btn1CallBack, System.Action btn2CallBack, string btn1Name, string btn2Name)
    {
        MsgTxt.text = msg;

        Btn1CallBack = btn1CallBack;
        Btn2CallBack = btn2CallBack;

        Btn1.SetActive(Btn1CallBack != null);
        Btn2.SetActive(Btn2CallBack != null);

        if (!string.IsNullOrEmpty(btn1Name))
            Btn1NameTxt.text = btn1Name;
        else
            Btn1NameTxt.text = "확인";

        if (!string.IsNullOrEmpty(btn2Name))
            Btn2NameTxt.text = btn2Name;
        else
            Btn2NameTxt.text = "취소";

        if (Btn2CallBack != null)
        {
            Btn1.transform.localPosition = new Vector3(-250f, Btn1.transform.localPosition.y, Btn1.transform.localPosition.z);
        }
        else
        {
            Btn1.transform.localPosition = new Vector3(0, Btn1.transform.localPosition.y, Btn1.transform.localPosition.z);
        }
    }

    public void OnClickBtn1()
    {
        if (Btn1CallBack != null)
            Btn1CallBack();

        Close();
    }

    public void OnClickBtn2()
    {
        if (Btn2CallBack != null)
            Btn2CallBack();

        Close();
    }
}
