﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UISettingPanel : UIBasePanel
{
    public GameObject CloseBtn, SaveBtn, ResetBtn, FAQBtn, GameExitBtn, GameInfoBtn;
    public Text BgTxt, SfxTxt;
    public Slider BGSlider, SFXSlider;

    public override void Init()
    {
        base.Init();

        SaveBtn.transform.Find("Text").GetComponent<Text>().text = "저장하기";
        ResetBtn.transform.Find("Text").GetComponent<Text>().text = "초기화";
        FAQBtn.transform.Find("Text").GetComponent<Text>().text = "FAQ";
        GameExitBtn.transform.Find("Text").GetComponent<Text>().text = "게임종료";
        GameInfoBtn.transform.Find("Text").GetComponent<Text>().text = "게임방법";
        BgTxt.text = "배경음악";
        SfxTxt.text = "효과음";
    }

    public override void LateInit()
    {
        base.LateInit();
    }

    public void OnClickCloseBtn()
    {
        UIMgr.instance.Prev();
    }

    public void OnClickSaveBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickResetBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickFAQBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickGameExitBtn()
    {
        UIMgr.instance.OpenPopup("게임을 종료 하시겠습니까?", () =>
        {
            Application.Quit();

        }, () => { }, "예", "아니오");
    }

    public void OnClickGameInfoBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void ChangeBGSlider()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void ChangeSFXlider()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }
}
