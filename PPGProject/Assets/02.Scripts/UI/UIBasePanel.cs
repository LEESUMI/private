﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIBasePanel : MonoBehaviour
{
    public PanelType PanelType = PanelType.Normal;

    private object[] parameters;
    public void SetParameters(params object[] param)
    {
        parameters = param;
    }

    public virtual void Init()
    {
    }

    public virtual void LateInit()
    {
        Show();
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
        UIMgr.instance.HideEvent(this);
    }

    public virtual void Close()
    {
        Destroy(gameObject);
        UIMgr.instance.CloseEvent(this);
    }

    public virtual PrevType GetPrevType()
    {
        return PrevType.Hide;
    }
}
