﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToastMessagePanel : MonoBehaviour
{
    public Lofle.Tween.UITweenColorAlpha TweenAlpha;
    public Text MsgTxt;

    public void Init(string msg)
    {
        gameObject.SetActive(true);
        MsgTxt.text = msg;
        TweenAlpha.PlayForward();
    }

    public void FinishAlphaEvent()
    {
        gameObject.SetActive(false);
    }
}
