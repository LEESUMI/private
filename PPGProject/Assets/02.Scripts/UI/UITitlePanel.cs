﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITitlePanel : UIBasePanel
{
    public Text DescriteTxt;
    private bool IsLoadingFinish = false;

    public override void Init()
    {
        base.Init();
    }

    public override void LateInit()
    {
        base.LateInit();

        IsLoadingFinish = false;
        StartCoroutine(_Loading());
    }

    IEnumerator _Loading()
    {
        DescriteTxt.text = "서비스 로고 업데이트 예정!";
        yield return new WaitForSeconds(1f);

        DescriteTxt.text = "개발사 로고 업데이트 예정!";
        yield return new WaitForSeconds(1f);

        DescriteTxt.text = "게임 데이터를 로딩 합니다...";
        yield return StartCoroutine(DataMgr.instance._LoadDataTable());
        yield return new WaitForSeconds(1f);

        DescriteTxt.text = "화면을 클릭하면 메인으로 이동 합니다";
        IsLoadingFinish = true;
    }

    public void OnClickGoMainBtn()
    {
        if (!IsLoadingFinish)
            return;

        UIMgr.instance.ShutDown();
        UIMgr.instance.OpenLoading(true);
        SceneMgr.instance.GoScene("MainScene", () =>
        {
            UIMgr.instance.Open("UILobbyPanel");

            UIMgr.instance.OpenLoading(false);
        });
    }
}
