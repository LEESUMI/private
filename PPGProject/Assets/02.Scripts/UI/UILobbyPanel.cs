﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILobbyPanel : UIBasePanel
{
    public GameObject[] AtkGages, DefGages, AspGages, CriGages, EvaGages, SpdGages;

    public override void Init()
    {
        base.Init();
    }

    public override void LateInit()
    {
        base.LateInit();

        SetStatusGage(AtkGages, 7);
        SetStatusGage(DefGages, 6);
        SetStatusGage(AspGages, 5);
        SetStatusGage(CriGages, 4);
        SetStatusGage(EvaGages, 3);
        SetStatusGage(SpdGages, 2);
    }

    private void SetStatusGage(GameObject[] arr, int gage)
    {
        for (int i = 0; i < arr.Length; ++i)
        {
            arr[i].SetActive(gage > i);
        }
    }

    public void OnClickCharacterBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickBookBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickAchievementBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickSaveBtn()
    {
        UIMgr.instance.OpenToastPopup("준비 중 입니다");
    }

    public void OnClickSettingBtn()
    {
        UIMgr.instance.Open("UISettingPanel");
    }

    public void OnClickStartBtn()
    {
        GameMgr.instance.StageInfo = DataMgr.instance.GetStageInfoById(10001);
        if (GameMgr.instance.StageInfo == null)
            return;

        UIMgr.instance.ShutDown();
        UIMgr.instance.OpenLoading(true);
        SceneMgr.instance.GoScene("Stage01Scene", () =>
        {
            //UIMgr.instance.Open("UILobbyPanel");

            UIMgr.instance.OpenLoading(false);
        });
    }
}
