﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleScene : MonoBehaviour
{
    private void Start()
    {
        GameMgr.instance.IsGameStart = true;
        UIMgr.instance.Open("UITitlePanel");
    }
}
