﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScene : MonoBehaviour
{
    private void Start()
    {
        if (!GameMgr.instance.IsGameStart)
        {
            SceneMgr.instance.GoScene("TitleScene", null);
            return;
        }
    }
}
